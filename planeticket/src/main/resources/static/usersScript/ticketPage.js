$(document).ready(function () {


    const searchParams = new URLSearchParams(window.location.search);
    $('.choose-place-btn').click(()=>{
        window.location.href = `http://localhost:8080/booking?id=${searchParams.get('id')}`;
    });
    $.ajax({
        url: `http://localhost:8080/ticket/findOne?id=${searchParams.get('id')}`,
        type: 'get',
        success: function (response) {
            addInfo(response);
        }
    });
    function addInfo(response) {
        $('#title').html(response.flyingFrom - response.flyingTo);
        $('#main-info').append(`
            <div class="item-inside-info"><h4>${response.flyingFrom}-${response.flyingTo}</h4></div>
     
        `);
        $('#info').append(`
            <div class="item-inside-description">
                Date: ${response.departing} - ${response.returning}
                 
            </div>
            <div>
            Description: ${response.description}
            </div>
        `)
    }
});
