$(document).ready(function() {
    let $contentTicket = $('.tickets-in-order');
    const searchParams = new URLSearchParams(window.location.search);
$.ajax({
    url: `http://localhost:8080/ticket/findOne?id=${searchParams.get('id')}`,
    type: 'get',
    success: function (response) {
        $contentTicket.append(` <div class = "ticket"><h4>${response.flyingFrom}-${response.flyingTo}</h4></div>`)
    }
    })
    $('#buy-ticket').click(() => {
        window.location.href = "http://localhost:8080/orderSuccessful";
    });

});