$(document).ready(function () {

    let $pageNumber = $('.page-number');
    let $pageSize = $('#pageSize');
    let $container = $('.container');
    let pages = 0;

        let getProducts = () => {
            let request = {
                flyingFrom: $('.from').val(),
                flyingTo: $('.to').val(),
                departing: $('.departing_date').val(),
                returning: $('.returning_date').val(),
                paginationRequest: {
                    page: +$pageNumber.html() -1,
                    size: $pageSize.val(),
                },

            };
            console.log(request);
            $.ajax({
                url: `http://localhost:8080/ticket/findByFilter`,
                type: 'post',
                contentType: "application/json",
                data: JSON.stringify(request),
                success: (response) => {
                    console.log(response);
                    pages = response.totalPages;
                    if (+$pageNumber.html() === pages) {
                        $('.btn-next').attr('disabled', 'true');
                    } else {
                        $('.btn-next').removeAttr('disabled');
                    }
                    appendProducts(response.data);
                },
                error: console.log
            })
            };
             getProducts();



            let appendProducts = (tickets) => {
                $container.html('');
                for (const ticket of tickets) {
                    addItem(ticket);
                }
            };
            let addItem = (ticket) => {
                $container.append(`
<div class="item">
                 <h4>${ticket.flyingFrom} - ${ticket.flyingTo}</h4>
                 <p>${ticket.departing} - ${ticket.returning}</p>
                 
                 <input class = "btn-details" type="button" value="Details" onClick='location.href="http://localhost:8080/ticketPage?id=${ticket.id}"'>
              
                 </div>
    
     
        `
                );
            };

        $('.btn-next').click(() => {
            let currentPage = +$pageNumber.html();
            if (currentPage < pages) {
                $pageNumber.html(currentPage + 1);
                getProducts();
            }
        });
        $('.btn-prev').click(() => {
            let currentPage = +$pageNumber.html();
            if (currentPage > 1) {
                $pageNumber.html(currentPage - 1);
                getProducts()
            }
        });
        $('#pageSize').change(() => {
            $pageSize = $('#pageSize');
            getProducts()
        });
    $('.btn-Searcher').click(() => {
        event.preventDefault();
        getProducts();

    
    });
    $('#clearOfFilterConfiguration').click(() => {
        $('#from_field').val('');
        $('#to_field').val('');
        $('#departing_field').val('');
        $('#returning_field').val('');
         });
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("search-flights").style.top = "0";
        } else {
            document.getElementById("search-flights").style.top = "-50px";
        }
    }
    });

