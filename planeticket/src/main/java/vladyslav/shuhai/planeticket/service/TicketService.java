package vladyslav.shuhai.planeticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import vladyslav.shuhai.planeticket.dto.request.TicketCriteria;
import vladyslav.shuhai.planeticket.dto.request.TicketRequest;
import vladyslav.shuhai.planeticket.dto.response.PageResponse;
import vladyslav.shuhai.planeticket.dto.response.TicketResponse;
import vladyslav.shuhai.planeticket.entity.Ticket;
import vladyslav.shuhai.planeticket.repository.TicketRepository;
import vladyslav.shuhai.planeticket.specification.TicketSpecification;
import java.util.stream.Collectors;

@Service
public class TicketService {
@Autowired
    private TicketRepository ticketRepository;

    public void create(TicketRequest request){
        ticketRepository.save(ticketRequestToTicket(null, request));
    }
    public PageResponse<TicketResponse> findByCriteria(TicketCriteria criteria) {
        Page<Ticket> page = ticketRepository.findAll(new TicketSpecification(criteria), criteria.getPaginationRequest().toPageable());
        return new PageResponse<>(page.getTotalPages(),
                page.getTotalElements(),
                page.get().map(TicketResponse::new).collect(Collectors.toList()));
        }

    private Ticket ticketRequestToTicket(Ticket ticket, TicketRequest request) {
        if (ticket == null) {
            ticket = new Ticket();
        }
        ticket.setFlyingFrom(request.getFlyingFrom());
        ticket.setDeparting(request.getDeparting());
        ticket.setFlyingTo(request.getFlyingTo());
        ticket.setReturning(request.getReturning());
        ticket.setDescription(request.getDescription());
        ticket.setEcoClassPrice(request.getEcoClassPrice());
        ticket.setFirstClassPrice(request.getFirstClassPrice());
        return ticket;
    }
    public Ticket findById(Long id) {
        return ticketRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Ticket with id=" + id + " not exists"));
    }
    public TicketResponse findOne(Long id){
        return new TicketResponse(findById(id));
    }
}
