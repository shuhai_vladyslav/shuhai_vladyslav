package vladyslav.shuhai.planeticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vladyslav.shuhai.planeticket.dto.request.PlaceRequest;
import vladyslav.shuhai.planeticket.entity.Place;
import vladyslav.shuhai.planeticket.repository.PlaceRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlaceService {
    @Autowired
    private PlaceRepository placeRepository;
    public Long reserveTicket(PlaceRequest request) {
        return save(null,request);
    }
    private Long save(Place place, PlaceRequest request) {
        if (place == null) {
            place = new Place();
        }
        place.setTicket(request.getTicket());
        place.setPlace(request.getPlace());

        placeRepository.save(place);
        return place.getId();
    }
    public List<String> findByTicketId(Long ticket){
       List<Place> tempPlace = placeRepository.findAllByTicket_Id(ticket);
        System.out.println(ticket);
        List<String> places = new ArrayList<>();
        for (Place place:tempPlace) {
            places.add(place.getPlace());
        }
        System.out.println(places);
        return places;
    }
    public Place findById(Long id) {
        return placeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Ticket with id=" + id + " not exists"));
    }

}
