package vladyslav.shuhai.planeticket.dto.response;

import lombok.Getter;
import lombok.Setter;
import vladyslav.shuhai.planeticket.entity.Place;
import vladyslav.shuhai.planeticket.entity.Ticket;

/**
 * Response from server in order to get place information.
 */
@Getter
@Setter
public class PlaceResponse {
    /**
     * Place id.
     */
    private Long id;
    /**
     * Place name.
     */
    private String place;
    /**
     * Ticket where the current place is.
     */
    private Ticket ticket;

    /**
     * Place response from server.
     *
     * @param place place object
     */
    public PlaceResponse(final Place place) {
        this.id = place.getId();
        this.place = place.getPlace();
        this.ticket = place.getTicket();
    }
}

