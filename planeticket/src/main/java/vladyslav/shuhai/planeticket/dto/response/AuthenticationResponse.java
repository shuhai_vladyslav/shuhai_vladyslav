package vladyslav.shuhai.planeticket.dto.response;


import lombok.Getter;
import lombok.Setter;

/**
 *  Response from server for user's authentication.
 */
@Getter
@Setter
public class AuthenticationResponse {
    /**
     * User's name.
     */
    private String username;
    /**
     * Token for a login session and identifies the user.
     */
    private String token;

    /**
     * Authentication response from server.
     * @param username user's name
     * @param token token
     */
    public AuthenticationResponse(final String username, final String token) {
        this.username = username;
        this.token = token;
    }

}
