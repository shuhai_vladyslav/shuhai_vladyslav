package vladyslav.shuhai.planeticket.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Criteria for ticket's filtration.
 */
@Getter
@Setter
public class TicketCriteria {
    /**
     * Departing date on ticket.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",
             timezone = "UTC")
    private Date departing;
    /**
     * Returning date on ticket.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",
             timezone = "UTC")
    private Date returning;
    /**
     * City of departure.
     */
    @NotNull
    private String flyingFrom;
    /**
     * City of arrival.
     */
    @NotNull
    private String flyingTo;
    /**
     * Ticket description.
     */
    private String description;
    /**
     * The price for a place in the first class.
     */
    private Integer firstClassPrice;
    /**
     * The price for a place in the second class.
     */
    private Integer ecoClassPrice;
    /**
     * Pagination request.
     */
    private PaginationRequest paginationRequest;

}
