package vladyslav.shuhai.planeticket.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import vladyslav.shuhai.planeticket.entity.Ticket;
import java.util.Date;

/**
 * Response from server in order to get ticket information.
 */
@Getter
@Setter
public class TicketResponse {
    /**
     * Departing date on ticket.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",
             timezone = "EET")
    private Date departing;
    /**
     * Returning date on ticket.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",
             timezone = "EET")
    private Date returning;
    /**
     * City of departure.
     */
    private String flyingFrom;
    /**
     * City of arrival.
     */
    private String flyingTo;
    /**
     * Ticket id.
     */
    private Long id;
    /**
     * Ticket description.
     */
    private String description;
    /**
     * The price for a place in the first class.
     */
    private Integer firstClassPrice;
    /**
     * The price for a place in the second class.
     */
    private Integer ecoClassPrice;

    /**
     * Ticket response from server.
     * @param ticket ticket object
     */
    public TicketResponse(final Ticket ticket) {
        this.flyingFrom = ticket.getFlyingFrom();
        this.flyingTo = ticket.getFlyingTo();
        this.departing = ticket.getDeparting();
        this.returning = ticket.getReturning();
        this.id = ticket.getId();
        this.description = ticket.getDescription();
        this.firstClassPrice = ticket.getFirstClassPrice();
        this.ecoClassPrice = ticket.getEcoClassPrice();
    }
}
