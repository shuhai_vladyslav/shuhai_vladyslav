package vladyslav.shuhai.planeticket.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.validation.constraints.NotNull;

/**
 * This is class which divided item into pages.
 */
@Getter
@Setter
public class PaginationRequest {
    /**
     * Count of item on one page.
     */
    @NotNull
    private Integer size;
    /**
     * Number of current page.
     */
    @NotNull
    private Integer page;
    /**
     * Sort direction.
     */
    @NotNull
    private Sort.Direction direction;
    /**
     * Field for sorting.
     */
    @NotNull
    private String field;

    /**
     * This is method which divided list of items into pages.
     * @return page
     */
    public final Pageable toPageable() {

        PageRequest pageRequest;
        if (direction != null && field != null) {
            pageRequest = PageRequest.of(page, size, direction, field);
        } else if (direction != null) {
            pageRequest = PageRequest.of(page, size, direction, "id");
        } else if (field != null) {
            pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, field);
        } else {
            pageRequest = PageRequest.of(page, size);
        }
        return pageRequest;
    }

}
