package vladyslav.shuhai.planeticket.dto.request;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import static vladyslav.shuhai.planeticket.tool.Constants.MIN_COUNT_SYMOBOL;
import static vladyslav.shuhai.planeticket.tool.Constants.MAX_COUNT_SYMOBOL;

/**
 * Request for user information.
 */
@Getter
@Setter
public class UserRequest {

    /**
     * User's login.
     */
    @NotBlank
    private String username;
    /**
     * User's password.
     */
    @Size(min = MIN_COUNT_SYMOBOL, max = MAX_COUNT_SYMOBOL)
    private String password;
    /**
     * User's first name.
     */
    private String firstName;
    /**
     * User's last name.
     */
    private String lastName;



}
