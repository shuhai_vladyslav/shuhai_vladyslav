package vladyslav.shuhai.planeticket.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Response from server for pagination.
 * @param <T> type of object.
 */
@Getter
@Setter
@AllArgsConstructor
public class PageResponse<T> {
    /**
     * Number of pages.
     */
    private Integer totalPages;
    /**
     * Number of all elements.
     */
    private Long totalElements;
    /**
     * List of pages.
     */
    private List<T> data;
}
