package vladyslav.shuhai.planeticket.dto.request;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

/**
 * Criteria for create request with places from database.
 */
@Getter
@Setter
public class PlaceCriteria {
    /**
     * List of places.
     */
    private List<String> places;
}
