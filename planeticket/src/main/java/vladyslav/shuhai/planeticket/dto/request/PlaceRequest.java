package vladyslav.shuhai.planeticket.dto.request;

import lombok.Getter;
import lombok.Setter;
import vladyslav.shuhai.planeticket.entity.Ticket;

/**
 * Request for place.
 */
@Getter
@Setter
public class PlaceRequest {
    /**
     * Ticket where the current place is.
     */
    private Ticket ticket;
    /**
     * Name of current place.
     */
    private String place;

}
