package vladyslav.shuhai.planeticket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @since 1.0
 * @author Shuhai Vladyslav
 * @version 1.0
 */

@SpringBootApplication
public class PlaneticketApplication {
    /**
     * This is the main method which run spring application.
     *
     * @param args Unused.
     */
    public static void main(final String[] args) {
        SpringApplication.run(PlaneticketApplication.class, args);
    }

    /**
     * Constructor.
     */
    private PlaneticketApplication() {

    }
}
