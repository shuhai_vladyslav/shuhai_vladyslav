package vladyslav.shuhai.planeticket.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;
/**
 * Create users table.
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(schema = "conshu", name = "users")
public class User {
    /**
     * User's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * User's login.
     */
    @Column(nullable = false, unique = true)
    private String username;
    /**
     * User's password.
     */
    private String password;
    /**
     * User's name.
     */
    private String firstName;
    /**
     * user's last name.
     */
    private String lastName;
    /**
     * password verification.
     */
    private String confirmPassword;
    /**
     * User's role (Admin/User).
     */
    @Column(nullable = false)
    private UserRole userRole;
    /**
     * Many to many relationship.
     * One user can have many tickets and in one ticket can be many users.
     */
    @ManyToMany
    private List<Ticket> tickets = new ArrayList<>();


}
