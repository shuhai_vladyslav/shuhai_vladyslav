package vladyslav.shuhai.planeticket.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Create table ticket.
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(schema = "conshu")
public class Ticket {
    /**
     * Ticket id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * Departing date on ticket.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",
             timezone = "UTC")
    private Date departing;
    /**
     * Returning date on ticket.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",
             timezone = "UTC")
    private Date returning;
    /**
     * City of departure.
     */
    private String flyingFrom;
    /**
     * City of arrival.
     */
    private String flyingTo;
    /**
     * Ticket description.
     */
    private String description;
    /**
     * The price for a place in the first class.
     */
    private Integer firstClassPrice;
    /**
     * The price for a place in the second class.
     */
    private Integer ecoClassPrice;
    /**
     * One to many relationship.
     * Save list of places in current ticket.
     */
    @OneToMany
    private List<Place> reservePlaces = new ArrayList<>();

    /**
     * Many to many relationship.
     * One ticket can be in many users and many users can be in one ticket.
     */
    @ManyToMany(mappedBy = "tickets")
    private List<User> users = new ArrayList<>();
}
