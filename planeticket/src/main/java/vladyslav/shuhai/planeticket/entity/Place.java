package vladyslav.shuhai.planeticket.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

/**
 * Create table place.
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(schema = "conshu")
public class Place {
    /**
     * Place id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * Place name.
     */
    private String place;
    /**
     * Many to one relationship with table ticket.
     * Many places can be in one ticket.
     */
    @ManyToOne
    private Ticket ticket;
}
