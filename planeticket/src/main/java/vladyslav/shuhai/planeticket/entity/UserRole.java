package vladyslav.shuhai.planeticket.entity;

/**
 * Enum for user's role.
 */
public enum UserRole {
    /**
     * User or admin.
     */
    ROLE_USER, ROLE_ADMIN
}
