package vladyslav.shuhai.planeticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import vladyslav.shuhai.planeticket.dto.request.PlaceRequest;
import vladyslav.shuhai.planeticket.service.PlaceService;

import javax.validation.Valid;
import java.util.List;

/**
 * This is controller which controls the reservation of seats.
 */
@RestController
@RequestMapping("/place")
public class PlaceController {
    /**
     * Auto wired to place service.
     */
    @Autowired
    private PlaceService placeService;

    /**
     * This is method which reserves places.
     * @param request request for place
     */
    @PostMapping("/reservePlace")
    public final void reserveTicket(
            @Valid @RequestBody final PlaceRequest request) {
        placeService.reserveTicket(request);
    }

    /**
     * This is method which get reserved places from database.
     * @param ticket id
     * @return list of places
     */
    @GetMapping("/getReservePlaces")
    public final List<String> getReservePlaces(final Long ticket) {
        return placeService.findByTicketId(ticket);
    }
}
