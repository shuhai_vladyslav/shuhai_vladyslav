package vladyslav.shuhai.planeticket.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import vladyslav.shuhai.planeticket.dto.request.UserRequest;
import vladyslav.shuhai.planeticket.dto.response.AuthenticationResponse;
import vladyslav.shuhai.planeticket.service.UserService;
import javax.validation.Valid;

/**
 * This is controller which manages user's information.
 */
@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {
    /**
     * Auto wired user service.
     */
    @Autowired
    private UserService userService;

    /**
     * This is method which return login method from userService by mapping.
     * @param request user request
     * @return login
     */
    @PostMapping("/login")
    public final AuthenticationResponse login(
            @Valid @RequestBody final UserRequest request) {
        return userService.login(request);
    }

    /**
     * This method which return register method from userService by mapping.
     * @param request user request
     * @return registration
     */
    @PostMapping("/register")
    public final AuthenticationResponse register(
            @Valid @RequestBody final UserRequest request) {
        return userService.register(request);
    }

}
