package vladyslav.shuhai.planeticket.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This is controller which indicating the path to html pages
 * by request mapping.
 */
@Controller
public class HtmlPageController {
    /**
     * This is method which return html page by request mapping.
     * @return registration page
     */
    @RequestMapping("/registration")
    public final String registration() {
        return "usersHtml/registration.html";
    }
    /**
     * This is method which return html page by request mapping.
     * @return authorization page
     */
    @RequestMapping("/authorization")
    public final String authorization() {
        return "usersHtml/authorization.html";
    }
    /**
     * This is method which return html page by request mapping.
     * @return page where you can search tickets
     */
    @RequestMapping("/ticketSearch")
    public final String ticketSearch() {
        return "usersHtml/ticketSearch.html";
    }
    /**
     * This is method which return html page by request mapping.
     * @return admin page where you can add new tickets
     */
    @RequestMapping("/addTicket")
    public final String addTicket() {
        return "addTicket.html";
    }
    /**
     * This is method which return html page by request mapping.
     * @return information about your ticket
     * @param id ticket
     */
    @RequestMapping("ticketPage")
    public final String ticketPage(final Long id) {
        return "usersHtml/ticket.html";
    }
    /**
     * This is method which return html page by request mapping.
     * @return page where you can choose place on flight
     * @param id ticket
     */
    @RequestMapping("booking")
    public final String booking(final Long id) {
        return "usersHtml/booking.html";
    }
    /**
     * This is method which return html page by request mapping.
     * @return information about your order
     * @param id ticket
     */
    @RequestMapping("order")
    public final String order(final Long id) {
        return "usersHtml/order.html";
    }
    /**
     * This is method which return html page by request mapping.
     * @return page with information about successful order
     */
    @RequestMapping("orderSuccessful")
    public final String orderSuccessful() {
        return "usersHtml/orderSuccessful.html";
    }


}
