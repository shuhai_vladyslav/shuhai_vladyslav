package vladyslav.shuhai.planeticket.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import vladyslav.shuhai.planeticket.dto.request.TicketCriteria;
import vladyslav.shuhai.planeticket.dto.request.TicketRequest;
import vladyslav.shuhai.planeticket.dto.response.PageResponse;
import vladyslav.shuhai.planeticket.dto.response.TicketResponse;
import vladyslav.shuhai.planeticket.service.TicketService;
import javax.validation.Valid;

/**
 * This is controller which manages tickets search.
 */
@RestController
@RequestMapping("/ticket")
public class TicketController {
    /**
     * Auto wired to ticket service.
     */
    @Autowired
    private TicketService ticketService;

    /**
     * This is method which searching tickets by customer filter.
     * @param ticketCriteria custom criteria
     * @return page with tickets
     */
    @PostMapping("/findByFilter")
    public final PageResponse<TicketResponse> findByFilter(
            @RequestBody final TicketCriteria ticketCriteria) {
        return ticketService.findByCriteria(ticketCriteria);
    }

    /**
     * This is method which find one ticket.
     * @param id ticket id
     * @return ticket
     */
    @GetMapping("/findOne")
    public final TicketResponse findOne(final Long id) {
        return ticketService.findOne(id);
    }

    /**
     * This is method which create ticket.
     * @param request ticket request
     */
    @PostMapping("/addTicket")
    public final void create(@Valid @RequestBody final TicketRequest request) {
        ticketService.create(request);
    }
}
