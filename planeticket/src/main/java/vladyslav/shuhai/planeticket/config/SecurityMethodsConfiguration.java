package vladyslav.shuhai.planeticket.config;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

/**
 * Security methods configuration.
 */
@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true
)
public class SecurityMethodsConfiguration
        extends GlobalMethodSecurityConfiguration {

}
