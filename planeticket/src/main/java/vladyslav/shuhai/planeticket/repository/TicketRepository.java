package vladyslav.shuhai.planeticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vladyslav.shuhai.planeticket.entity.Ticket;

/**
 * Repository for ticket.
 */
public interface TicketRepository extends JpaRepository<Ticket, Long>,
        JpaSpecificationExecutor<Ticket> {

}
