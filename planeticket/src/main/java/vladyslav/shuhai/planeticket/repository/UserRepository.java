package vladyslav.shuhai.planeticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vladyslav.shuhai.planeticket.entity.User;
import java.util.Optional;

/**
 * Repository for user.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * Finding user by login.
     * @param username user's login
     * @return optional of users
     */
    Optional<User> findByUsername(String username);

    /**
     *  Checking if user exists.
     * @param username user's login
     * @return true if user exists
     */
    boolean existsByUsername(String username);
}
