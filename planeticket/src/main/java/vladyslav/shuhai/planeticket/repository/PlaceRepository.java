package vladyslav.shuhai.planeticket.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import vladyslav.shuhai.planeticket.entity.Place;

import java.util.List;

/**
 *  Repository for place.
 */
public interface PlaceRepository extends CrudRepository<Place, Long>,
        JpaSpecificationExecutor<Place> {
    /**
     * Finding all places on some flight.
     * @param ticket ticket id
     * @return list of all places on some flight
     */
    List<Place> findAllByTicket_Id(Long ticket);
}
