package vladyslav.shuhai.planeticket.specification;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.jpa.domain.Specification;
import vladyslav.shuhai.planeticket.dto.request.TicketCriteria;
import vladyslav.shuhai.planeticket.entity.Ticket;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

public class TicketSpecification implements Specification<Ticket> {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone = "UTC")
    private Date departing;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone = "UTC")
    private Date returning;
    @NotEmpty
    private String flyingFrom;
    @NotEmpty
    private String flyingTo;
    public TicketSpecification(TicketCriteria criteria){
        departing = criteria.getDeparting();
        returning = criteria.getReturning();
        flyingFrom = criteria.getFlyingFrom();
        flyingTo = criteria.getFlyingTo();
    }
    @Override
    public Predicate toPredicate(Root<Ticket> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Predicate byFrom = findByFlyingFrom(root,criteriaBuilder);
        Predicate byTo = findByFlyingTo(root,criteriaBuilder);
        Predicate byDeparting = findByDeparting(root,criteriaBuilder);
        Predicate byReturning = findByReturning(root,criteriaBuilder);

        return criteriaBuilder.and(byFrom,byTo,byDeparting,byReturning);
    }

    private Predicate findByReturning(Root<Ticket> root, CriteriaBuilder criteriaBuilder) {
        Predicate predicate;
        if(returning!=null){
            predicate = criteriaBuilder.equal(root.get("returning"),returning );
        }
        else {
            predicate = criteriaBuilder.conjunction();
        }
        return predicate;
    }

    private Predicate findByDeparting(Root<Ticket> root, CriteriaBuilder criteriaBuilder) {
        Predicate predicate;
        if(departing!=null){
            predicate = criteriaBuilder.equal(root.get("departing"),departing);
        }
        else {
            predicate = criteriaBuilder.conjunction();
        }
        return predicate;
    }

    private Predicate findByFlyingTo(Root<Ticket> root, CriteriaBuilder criteriaBuilder) {
        Predicate predicate;
        if (flyingTo != null) {
            predicate = criteriaBuilder.like(root.get("flyingTo"), '%' + flyingTo + '%');
        } else {
            predicate = criteriaBuilder.conjunction();
        }
        return predicate;
    }

    private Predicate findByFlyingFrom(Root<Ticket> root, CriteriaBuilder criteriaBuilder) {
        Predicate predicate;
        if (flyingFrom != null) {
            predicate = criteriaBuilder.like(root.get("flyingFrom"), '%' + flyingFrom + '%');
        } else {
            predicate = criteriaBuilder.conjunction();
        }
        return predicate;
    }
}
